package com.race.raceapp.view

import android.content.Context
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.race.raceapp.R

class CustomHeaderView(context: Context) : ConstraintLayout(context) {

    init {
        val view = View.inflate(context, R.layout.custom_header, this)
    }
}