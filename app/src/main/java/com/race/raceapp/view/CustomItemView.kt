package com.race.raceapp.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.race.raceapp.R
import kotlinx.android.synthetic.main.custom_view.view.*

class CustomItemView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private var img: Drawable?
    private var name: String?
    private var description: String?

    init {
        val view = View.inflate(context, R.layout.custom_view, this)
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CustomItemView,
            0, 0
        ).apply {
            try {
                img = getDrawable(R.styleable.CustomItemView_image)
                name = getString(R.styleable.CustomItemView_name)
                description = getString(R.styleable.CustomItemView_description)
                view.imageView.setImageDrawable(img)
                view.tvName.text = name
                view.tvDescription.text = description
            } finally {
                recycle()
            }
        }
    }

    fun getImage(): Drawable? {
        return img
    }

    fun setImage(img: Drawable?) {
        this.img = img
        imageView.setImageDrawable(img)
        invalidate()
        requestLayout()
    }

    fun getName() : String? {
        return name;
    }

    fun setName(name: String?) {
        this.name = name
        tvName.text = name
        invalidate()
        requestLayout()
    }

    fun getDescription() : String? {
        return description;
    }

    fun setDescription(description: String?) {
        this.description = description
        tvDescription.text = description
        invalidate()
        requestLayout()
    }
}
