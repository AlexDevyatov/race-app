package com.race.raceapp.utils

import com.race.raceapp.db.model.Event
import com.race.raceapp.db.model.EventWithTracks

fun Float.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return kotlin.math.round(this * multiplier) / multiplier
}

fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return kotlin.math.round(this * multiplier) / multiplier
}

fun List<EventWithTracks>.createEventsList() : List<Event> {
    val result: MutableList<Event> = mutableListOf()
    for (eventWithTracks in this) {
        result.add(eventWithTracks.event)
    }
    return result
}