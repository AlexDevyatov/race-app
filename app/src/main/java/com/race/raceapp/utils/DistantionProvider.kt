package com.race.raceapp.utils

internal object DistantionProvider {

    var mainDistantionMeter = 0.0

    var shortDistantionMeter = 0.0

    var lastSpeedKmH = 0.0

    fun resetMainDistantion() {
        mainDistantionMeter = 0.0
        shortDistantionMeter = 0.0
    }

    fun resetShortDistantion() {
        shortDistantionMeter = 0.0
    }

    fun addDistantion(dist: Float) {
        mainDistantionMeter += dist
        shortDistantionMeter += dist
    }

    fun getMainDistantionKm() = (mainDistantionMeter / 1000.0).round(2)

    fun getShortDistantionKm() = (shortDistantionMeter / 1000.0).round(2)

    fun getSpeedKmH(speedMS : Double) : Double {
        lastSpeedKmH = (speedMS * 3.6).round(2)
        return lastSpeedKmH
    }

    fun clear() {
        mainDistantionMeter = 0.0
        shortDistantionMeter = 0.0
        lastSpeedKmH = 0.0
    }
}