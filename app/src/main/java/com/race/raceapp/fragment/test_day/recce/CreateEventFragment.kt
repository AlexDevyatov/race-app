package com.race.raceapp.fragment.test_day.recce

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.db.AppDatabase
import com.race.raceapp.db.model.Event
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_create_event.*
import java.text.SimpleDateFormat

class CreateEventFragment : Fragment() {

    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSaveEvent.setOnClickListener {
            val db = AppDatabase.getInstance(activity!!)

            val formatter = SimpleDateFormat("dd/MM/yyyy")
            val startDate = formatter.parse(etStartDate.text.toString())
            val finishDate = formatter.parse(etFinishDate.text.toString())
            val event = Event(
                null,
                eventName = etEventName.text.toString(),
                startDate = startDate,
                finishDate = finishDate,
                country = etCountry.text.toString(),
                region = etRegion.text.toString()
            )
            disposable.add(Completable.fromAction {
                db.dao().insertEvent(event)
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Toast.makeText(activity, "Event saved succesfully", Toast.LENGTH_SHORT).show()
                    activity?.onBackPressed()
                })
        }
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}