package com.race.raceapp.fragment.test_day.race

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.activity.MainActivity
import com.race.raceapp.activity.race.SaveSetupActivity
import com.race.raceapp.db.AppDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_save_setup.*

class SaveSetupFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_save_setup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnSaveSetup.setOnClickListener {
            if (etSetupName.text.isNullOrEmpty()) {
                Toast.makeText(activity!!, R.string.please_input_the_name_of_setup, Toast.LENGTH_SHORT).show()
            } else {
                (activity as SaveSetupActivity).setSetupName(etSetupName.text.toString())
                (activity as SaveSetupActivity).saveSetupAndRun()
            }
        }
    }
}