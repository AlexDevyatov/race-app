package com.race.raceapp.fragment.test_day

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.race.raceapp.R
import com.race.raceapp.adapter.TestRecceAdapter
import com.race.raceapp.db.AppDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_test_recce.*

class TestRecceFragment : Fragment() {

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("TestRecceFragment", "OnCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_test_recce, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvTracksTestRecce.layoutManager = LinearLayoutManager(activity)
        rvTracksTestRecce.adapter = TestRecceAdapter(activity!!)
        val db = AppDatabase.getInstance(activity!!)
        disposable.add(
            db.dao().loadTracks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        (rvTracksTestRecce.adapter as TestRecceAdapter).tracks = it
                    },
                    {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                    }
                )
        )
    }

    override fun onStart() {
        Log.d("TestRecceFragment", "OnStart")

        super.onStart()
    }

    override fun onPause() {
        Log.d("TestRecceFragment", "OnPause")

        super.onPause()
    }

    override fun onStop() {
        Log.d("TestRecceFragment", "OnStop")

        super.onStop()
    }

    override fun onDestroy() {
        Log.d("TestRecceFragment", "OnDestroy")
        disposable.dispose()
        super.onDestroy()
    }
}