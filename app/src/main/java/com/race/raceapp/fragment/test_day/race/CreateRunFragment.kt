package com.race.raceapp.fragment.test_day.race

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.activity.race.CreateRunActivity
import com.race.raceapp.activity.race.SaveSetupActivity
import kotlinx.android.synthetic.main.fragment_create_run.*

class CreateRunFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_run, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if ((activity as CreateRunActivity).editRunMode) {
            etRunNumber.setText((activity as CreateRunActivity).run.number?.toString())
            etRunKm.setText((activity as CreateRunActivity).run.km?.toString())
            etTimeIn.setText((activity as CreateRunActivity).run.timeIn?.toString())
            etFuelIn.setText((activity as CreateRunActivity).run.fuelIn?.toString())
            etFuelAdd.setText((activity as CreateRunActivity).run.fuelAdd?.toString())
            etWeather.setText((activity as CreateRunActivity).run.weather)
            etTireType.setText((activity as CreateRunActivity).run.tyreType)
            etTirePressure.setText((activity as CreateRunActivity).run.tyrePressure)
            etComment.setText((activity as CreateRunActivity).run.comment?.toString())
        }

        etRunNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty()) {
                    val num = s.toString().toInt()
                    (activity as CreateRunActivity).run.number = num
                } else {
                    (activity as CreateRunActivity).run.number = null
                }
            }
        })

        btnSaveRun.setOnClickListener {
            if (!etRunKm.text.isNullOrEmpty())
                (activity as CreateRunActivity).run.km = etRunKm.text.toString().toDouble()
            if (!etTimeIn.text.isNullOrEmpty())
                (activity as CreateRunActivity).run.timeIn = etTimeIn.text.toString().toDouble()
            if (!etFuelIn.text.isNullOrEmpty())
                (activity as CreateRunActivity).run.fuelIn = etFuelIn.text.toString().toDouble()
            if (!etFuelAdd.text.isNullOrEmpty())
                (activity as CreateRunActivity).run.fuelAdd = etFuelAdd.text.toString().toDouble()
            (activity as CreateRunActivity).run.weather = etWeather.text.toString()
            (activity as CreateRunActivity).run.tyreType = etTireType.text.toString()
            (activity as CreateRunActivity).run.tyrePressure = etTirePressure.text.toString()
            (activity as CreateRunActivity).run.comment = etComment.text.toString()

            val intent = Intent(activity!!, SaveSetupActivity::class.java)
            intent.putExtra("setup", (activity as CreateRunActivity).setup)
            intent.putExtra("run", (activity as CreateRunActivity).run)
            startActivity(intent)
        }
    }
}