package com.race.raceapp.fragment.test_day

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.race.raceapp.R
import com.race.raceapp.adapter.TestRaceAdapter
import com.race.raceapp.db.AppDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_test_race.*

class TestRaceFragment : Fragment() {

    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_test_race, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvTracksTestRace.layoutManager = LinearLayoutManager(activity!!)
        rvTracksTestRace.adapter = TestRaceAdapter(activity!!)

        val db = AppDatabase.getInstance(activity!!)
        disposable.add(
            db.dao().loadTracksWithRunsAndSetups()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            tvNoTracks.visibility = View.GONE
                        }
                        (rvTracksTestRace.adapter as TestRaceAdapter).tracks = it
                    },
                    {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                    }
                ))
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}