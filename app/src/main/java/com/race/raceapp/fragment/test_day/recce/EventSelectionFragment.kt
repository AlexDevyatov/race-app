package com.race.raceapp.fragment.test_day.recce

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.activity.MainActivity
import com.race.raceapp.activity.recce.CreateEventActivity
import com.race.raceapp.activity.recce.EventSelectionActivity
import com.race.raceapp.db.AppDatabase
import com.race.raceapp.db.model.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_event_selection.*
import kotlinx.android.synthetic.main.fragment_event_selection.btnSaveTrack


class EventSelectionFragment : Fragment() {

    private var event: Event? = null
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_event_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = emptyList<Event>()
        var adapter = ArrayAdapter<Event>(activity!!, R.layout.support_simple_spinner_dropdown_item, list)
        spinner.setAdapter(adapter)

        val db = AppDatabase.getInstance(activity!!)
        disposable.add(db.dao().loadEvents()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                adapter = ArrayAdapter<Event>(activity!!, R.layout.support_simple_spinner_dropdown_item, it)
                spinner.setAdapter(adapter)
            })

        btnNewEvent.setOnClickListener {
            val intent = Intent(activity!!, CreateEventActivity::class.java)
            startActivity(intent)
        }

        spinner.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val item = parent?.getItemAtPosition(position)
                if (item is Event) {
                    event = item
                }
            }

        btnSaveTrack.setOnClickListener {
            if (event != null) {
                val db = AppDatabase.getInstance(activity!!)
                val track = (activity as EventSelectionActivity).track
                track.eventId = event?.id
                disposable.add(db.dao().updateTrack(track)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        Toast.makeText(activity, "Track saved succesfully", Toast.LENGTH_SHORT).show()
                        val intent = Intent(activity!!, MainActivity::class.java)
                        startActivity(intent)
                    })
            } else {
                Toast.makeText(activity!!, R.string.no_event_alert, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}