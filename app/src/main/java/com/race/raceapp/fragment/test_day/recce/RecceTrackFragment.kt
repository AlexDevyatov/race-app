package com.race.raceapp.fragment.test_day.recce

import android.content.Context
import android.content.Intent
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.activity.recce.EventSelectionActivity
import com.race.raceapp.activity.recce.RecceTrackActivity
import com.race.raceapp.utils.DistantionProvider
import kotlinx.android.synthetic.main.fragment_recce_track.*
import java.text.SimpleDateFormat
import java.util.*

class RecceTrackFragment : Fragment() {

    private val SHORT_DURATION = 250L
    private val LONG_DURATION = 500L

    private val handler = Handler(Looper.getMainLooper())
    private val runnable = object : Runnable {
        override fun run() {
            if (tvCurrentTime != null) {
                tvCurrentTime.text = SimpleDateFormat("HH:mm:ss", Locale.US).format(Date())
                handler.postDelayed(this, 1000)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recce_track, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handler.postDelayed(runnable, 10)

        btnStart.setOnClickListener {
            btnStart.visibility = View.GONE
            btnFinish.visibility = View.VISIBLE
            (activity as RecceTrackActivity).bindService()
            (activity as RecceTrackActivity).registerReceiver()
        }

        btnFinish.setOnClickListener {
            (activity as RecceTrackActivity).stopService()
            btnFinish.visibility = View.GONE
            if ((activity as RecceTrackActivity).mayReturnTrack) {
                btnCreateReturnTrack.visibility = View.VISIBLE
                (activity as RecceTrackActivity).mayReturnTrack = false
            }
            btnSave.visibility = View.VISIBLE
        }

        btnSave.setOnClickListener {
            val track = (activity as RecceTrackActivity).track
            val intent = Intent(activity!!, EventSelectionActivity::class.java)
            intent.putExtra("track", track)
            startActivity(intent)
        }

        btnCreateReturnTrack.setOnClickListener {
            btnStart.visibility = View.VISIBLE
            btnCreateReturnTrack.visibility = View.GONE
            btnSave.visibility = View.GONE
        }

        tvShortTrip.text = "${DistantionProvider.getShortDistantionKm()}"
        tvMainTrip.text = "${DistantionProvider.getMainDistantionKm()}"
        tvSpeed.text = "${DistantionProvider.lastSpeedKmH}"

        rootLayout.setOnClickListener {
            DistantionProvider.resetShortDistantion()
            tvShortTrip.text = "${DistantionProvider.getShortDistantionKm()}"
            val vibrator = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createOneShot(SHORT_DURATION, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                vibrator.vibrate(SHORT_DURATION)
            }
        }
        rootLayout.setOnLongClickListener {
            DistantionProvider.resetMainDistantion()
            tvMainTrip.text = "${DistantionProvider.getMainDistantionKm()}"
            val vibrator = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createOneShot(LONG_DURATION, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                vibrator.vibrate(LONG_DURATION)
            }
            false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }

}