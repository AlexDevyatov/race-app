package com.race.raceapp.fragment.test_day.recce

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.db.AppDatabase
import com.race.raceapp.db.model.Track
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_create_new_track.*

class CreateNewTrackFragment : Fragment() {

    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_new_track, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSaveTrack.setOnClickListener {
            val db = AppDatabase.getInstance(activity!!)
            disposable.add(Completable.fromAction {
                db.dao().insertTrack(Track(null, etTrackName.text.toString(), null))
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Toast.makeText(activity, "Track saved succesfully", Toast.LENGTH_SHORT).show()
                    activity?.onBackPressed()
                })
        }
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

}