package com.race.raceapp.fragment.test_day.race

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.activity.race.CreateRunActivity
import com.race.raceapp.activity.race.SaveSetupActivity
import com.race.raceapp.db.AppDatabase
import com.race.raceapp.db.model.Setup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_create_setup.*

class CreateSetupFragment : Fragment() {

    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_setup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ArrayAdapter<Setup>(activity!!, R.layout.support_simple_spinner_dropdown_item, emptyList())
        setupNameSpinner.setAdapter(adapter)

        val db = AppDatabase.getInstance(activity!!)
        disposable.add(db.dao().loadSetups()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                val adapter = ArrayAdapter<Setup>(activity!!, R.layout.support_simple_spinner_dropdown_item, it)
                setupNameSpinner.setAdapter(adapter)
            })

        setupNameSpinner.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val item = parent?.getItemAtPosition(position)
                if (item is Setup) {
                    fillFields(item)
                }
            }

        if ((activity as CreateRunActivity).editRunMode) {
            fillFields((activity as CreateRunActivity).setup)
        }

        clTechnicalTitle.setOnClickListener {
            if (llTechnical.visibility == View.GONE) {
                llTechnical.visibility = View.VISIBLE
                ivTechnical.setBackgroundResource(R.drawable.arrow_up)
            } else {
                llTechnical.visibility = View.GONE
                ivTechnical.setBackgroundResource(R.drawable.arrow_down)
            }
        }

        clAdditionalTitle.setOnClickListener {
            if (llAdditional.visibility == View.GONE) {
                llAdditional.visibility = View.VISIBLE
                ivAdditional.setBackgroundResource(R.drawable.arrow_up)
            } else {
                llAdditional.visibility = View.GONE
                ivAdditional.setBackgroundResource(R.drawable.arrow_down)
            }
        }

        btnSaveSetup.setOnClickListener {
            if ((activity as CreateRunActivity).run.number == null) {
                Toast.makeText(
                    activity!!,
                    R.string.please_input_the_number_of_run,
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                (activity as CreateRunActivity).setup.setupId = null
                (activity as CreateRunActivity).setup.setupName = setupNameSpinner.text.toString()
                if (!etRideHeightFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.rideHeightFront =
                    etRideHeightFront.text.toString().toDouble()
                if (!etRideHeightRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.rideHeightRear =
                    etRideHeightRear.text.toString().toDouble()
                if (!etSpringsFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.springsFront =
                    etSpringsFront.text.toString().toDouble()
                if (!etSpringsRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.springsRear =
                    etSpringsRear.text.toString().toDouble()
                if (!etSpringLengthFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.springLengthFront =
                    etSpringLengthFront.text.toString().toDouble()
                if (!etSpringLengthRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.springLengthRear =
                    etSpringLengthRear.text.toString().toDouble()
                if (!etCamberFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.camberFront =
                    etCamberFront.text.toString().toDouble()
                if (!etCamberRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.camberRear =
                    etCamberRear.text.toString().toDouble()
                (activity as CreateRunActivity).setup.toeFront = etToeFront.text.toString()
                (activity as CreateRunActivity).setup.toeRear = etToeRear.text.toString()
                (activity as CreateRunActivity).setup.dampersModelFront =
                    etDampersFront.text.toString()
                (activity as CreateRunActivity).setup.dampersModelRear =
                    etDampersRear.text.toString()
                if (!etReboundFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.clicksReboundFront =
                    etReboundFront.text.toString().toDouble()
                if (!etReboundRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.clicksReboundRear =
                    etReboundRear.text.toString().toDouble()
                if (!etBumpFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.clicksBumpFront =
                    etBumpFront.text.toString().toDouble()
                if (!etBumpRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.clicksBumpRear =
                    etBumpRear.text.toString().toDouble()
                if (!etFustBumpFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.clicksFustBumpFront =
                    etFustBumpFront.text.toString().toDouble()
                if (!etFustBumpRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.clicksFustBumpRear =
                    etFustBumpRear.text.toString().toDouble()
                (activity as CreateRunActivity).setup.rampsFront = etRampsFront.text.toString()
                (activity as CreateRunActivity).setup.rampsRear = etRampsRear.text.toString()
                (activity as CreateRunActivity).setup.ffFront = etFFFront.text.toString()
                (activity as CreateRunActivity).setup.ffRear = etFFRear.text.toString()
                if (!etPreloadFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.preloadFront =
                    etPreloadFront.text.toString().toDouble()
                if (!etFustBumpRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.preloadRear =
                    etFustBumpRear.text.toString().toDouble()
                if (!etBalanceFront.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.balanceFront =
                    etBalanceFront.text.toString().toDouble()
                if (!etBalanceRear.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.balanceRear =
                    etBalanceRear.text.toString().toDouble()
                (activity as CreateRunActivity).setup.brakePads = etBrakePads.text.toString()
                if (!etFuelLevel.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.fuelLevel =
                    etFuelLevel.text.toString().toDouble()
                (activity as CreateRunActivity).setup.tyres = etTyres.text.toString()
                if (!etTemperature.text.isNullOrEmpty()) (activity as CreateRunActivity).setup.temperature =
                    etTemperature.text.toString().toDouble()
                (activity as CreateRunActivity).setup.time = etTime.text.toString()

                val intent = Intent(activity!!, SaveSetupActivity::class.java)
                intent.putExtra("setup", (activity as CreateRunActivity).setup)
                intent.putExtra("run", (activity as CreateRunActivity).run)
                startActivity(intent)
            }
        }
    }

    private fun fillFields(setup: Setup) {
        setupNameSpinner.setText(setup.setupName)
        etRideHeightFront.setText(setup.rideHeightFront?.toString())
        etRideHeightRear.setText(setup.rideHeightRear?.toString())
        etSpringsFront.setText(setup.springsFront?.toString())
        etSpringsRear.setText(setup.rideHeightRear?.toString())
        etSpringLengthFront.setText(setup.springLengthFront?.toString())
        etSpringLengthRear.setText(setup.springLengthRear?.toString())
        etCamberFront.setText(setup.camberFront?.toString())
        etCamberRear.setText(setup.camberRear?.toString())
        etToeFront.setText(setup.toeFront)
        etToeRear.setText(setup.toeRear)
        etSpringLengthFront.setText(setup.springLengthFront?.toString())
        etDampersFront.setText(setup.dampersModelFront)
        etDampersRear.setText(setup.dampersModelRear)
        etReboundFront.setText(setup.clicksReboundFront?.toString())
        etReboundRear.setText(setup.clicksReboundRear?.toString())
        etBumpFront.setText(setup.clicksBumpFront?.toString())
        etBumpRear.setText(setup.clicksBumpRear?.toString())
        etFustBumpFront.setText(setup.clicksFustBumpFront?.toString())
        etFustBumpRear.setText(setup.clicksFustBumpRear?.toString())
        etRampsFront.setText(setup.rampsFront)
        etRampsRear.setText(setup.rampsRear)
        etFFFront.setText(setup.ffFront)
        etFFRear.setText(setup.ffRear)
        etPreloadFront.setText(setup.preloadFront?.toString())
        etPreloadRear.setText(setup.preloadRear?.toString())
        etBalanceFront.setText(setup.balanceFront?.toString())
        etBalanceRear.setText(setup.balanceRear?.toString())
        etBrakePads.setText(setup.brakePads)
        etFuelLevel.setText(setup.fuelLevel?.toString())
        etTyres.setText(setup.tyres)
        etTemperature.setText(setup.temperature?.toString())
        etTime.setText(setup.time?.toString())
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}