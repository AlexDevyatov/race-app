package com.race.raceapp.fragment.test_day


import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

import com.race.raceapp.R
import com.race.raceapp.activity.TestDayActivity
import com.race.raceapp.adapter.CustomFragmentPagerAdapter
import kotlinx.android.synthetic.main.fragment_test_day.*

/**
 * A simple [Fragment] subclass.
 */
class TestDayFragment : Fragment() {

    private lateinit var pagerAdapter: CustomFragmentPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("TestDayFragment", "OnCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test_day, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager()
        testDayViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> setRecceColors()
                    1 -> setRaceColors()
                }
            }

        })
    }

    private fun setRecceColors() {
        (activity as TestDayActivity).setToolbarColor(ColorDrawable(resources.getColor(R.color.colorToolbarBackgroundTestRecce)))
        testDayTabsFragment.background = ColorDrawable(resources.getColor(R.color.colorToolbarBackgroundTestRecce))
        testDayTabsFragment.setSelectedTabIndicatorColor(resources.getColor(R.color.colorToolbarBackgroundTestRecce))
    }

    private fun setRaceColors() {
        (activity as TestDayActivity).setToolbarColor(ColorDrawable(resources.getColor(R.color.colorToolbarBackgroundTestRace)))
        testDayTabsFragment.background = ColorDrawable(resources.getColor(R.color.colorToolbarBackgroundTestRace))
        testDayTabsFragment.setSelectedTabIndicatorColor(resources.getColor(R.color.colorToolbarBackgroundTestRace))
    }

    private fun setupViewPager() {
        pagerAdapter = CustomFragmentPagerAdapter(childFragmentManager)
        pagerAdapter.addFragment(TestRecceFragment(), getString(R.string.test_recce))
        pagerAdapter.addFragment(TestRaceFragment(), getString(R.string.test_race))
        testDayViewPager.adapter = pagerAdapter
        testDayTabsFragment.setupWithViewPager(testDayViewPager)
        val titles = listOf(getString(R.string.test_recce), getString(R.string.test_race))
        testDayTabsFragment.setTitlesAtTabs(titles)
    }

    override fun onPause() {
        Log.d("TestDayFragment", "OnPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d("TestDayFragment", "OnStop")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d("TestDayFragment", "OnDestroy")
        pagerAdapter.removeAll()
        super.onDestroy()
    }

}
