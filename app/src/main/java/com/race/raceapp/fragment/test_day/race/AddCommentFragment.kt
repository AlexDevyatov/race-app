package com.race.raceapp.fragment.test_day.race

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import com.race.raceapp.activity.race.AddCommentActivity
import com.race.raceapp.db.AppDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_comment.*

class AddCommentFragment : Fragment() {

    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_comment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnSaveRun.setOnClickListener {
            if (etComment.text.isNullOrEmpty()) {
                activity!!.finish()
            } else {
                val db = AppDatabase.getInstance(activity!!)
                (activity as AddCommentActivity).run.comment = etComment.text.toString()
                db.dao().updateRun((activity as AddCommentActivity).run)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        Toast.makeText(activity, "Run updated successfully", Toast.LENGTH_SHORT).show()
                        activity!!.finish()
                    }
            }
        }
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}