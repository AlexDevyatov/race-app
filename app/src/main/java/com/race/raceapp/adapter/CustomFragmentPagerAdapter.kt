package com.race.raceapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class CustomFragmentPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val tabTitles = mutableListOf<String>()
    private val mFragments = mutableListOf<Fragment>()

    fun addFragment(fragment: Fragment, title: String) {
        mFragments.add(fragment)
        tabTitles.add(title)
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment = mFragments[position]

    override fun getCount(): Int = mFragments.size

    override fun getPageTitle(position: Int): CharSequence? = tabTitles[position]

    fun removeAll() {
        tabTitles.clear()
        mFragments.clear()
        notifyDataSetChanged()
    }
}