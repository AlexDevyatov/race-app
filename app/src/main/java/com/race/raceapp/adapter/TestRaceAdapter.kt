package com.race.raceapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.race.raceapp.R
import com.race.raceapp.activity.race.CreateRunActivity
import com.race.raceapp.db.AppDatabase
import com.race.raceapp.db.model.TrackWithRunsAndSetups
import kotlinx.android.synthetic.main.item_test_race.view.*

class TestRaceAdapter() : RecyclerView.Adapter<TestRaceAdapter.ViewHolder>() {

    var tracks: List<TrackWithRunsAndSetups> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var context: Context

    lateinit var db: AppDatabase

    constructor(context: Context) : this() {
        this.context = context
        this.db = AppDatabase.getInstance(context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTrackNumber: TextView? = view.tvTrackNumber
        val tvTrackName: TextView? = view.tvTrackName
        val tvEventName: TextView? = view.tvEventName
        val btnAddRun: ImageView? = view.btnAddRun
        val rvRuns: RecyclerView? = view.rvRuns
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.item_test_race, parent, false)
    )

    override fun getItemCount(): Int = tracks.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val track = tracks[position]

        holder.tvTrackNumber?.text = track.track.trackId.toString()
        holder.tvTrackName?.text = track.track.name
        holder.tvEventName?.text = track.track.eventId.toString() //TODO времянка!!
        holder.btnAddRun?.setOnClickListener {
            val intent = Intent(context, CreateRunActivity::class.java)
            intent.putExtra("track_id", track.track.trackId)
            context.startActivity(intent)
        }

        holder.rvRuns?.layoutManager = LinearLayoutManager(context)
        holder.rvRuns?.adapter = RunsAdapter(context, track.runs)
    }
}