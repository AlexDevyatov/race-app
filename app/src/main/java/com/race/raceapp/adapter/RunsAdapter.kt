package com.race.raceapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.race.raceapp.R
import com.race.raceapp.activity.race.AddCommentActivity
import com.race.raceapp.activity.race.CreateRunActivity
import com.race.raceapp.db.model.Run
import com.race.raceapp.db.model.RunWithSetup
import kotlinx.android.synthetic.main.item_run.view.*

class RunsAdapter(var context: Context, var runs: List<RunWithSetup>) :
    RecyclerView.Adapter<RunsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val btnRun: Button? = view.btnRun
        val llAddComment: LinearLayout? = view.llAddComment
        val clComment: ConstraintLayout? = view.clComment
        val tvComment: TextView? = view.tvComment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.item_run, parent, false)
    )

    override fun getItemCount(): Int = runs.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val run = runs[position]
        holder.btnRun?.text = "Run ${run.run.number}"
        holder.btnRun?.setOnClickListener {
            val intent = Intent(context, CreateRunActivity::class.java)
            intent.putExtra("run", run)
            context.startActivity(intent)
        }
        if (run.run.comment.isNullOrEmpty()) {
            holder.llAddComment?.visibility = View.VISIBLE
            holder.llAddComment?.setOnClickListener {
                Toast.makeText(context, "Add comment", Toast.LENGTH_SHORT).show()
            }
            holder.clComment?.visibility = View.GONE
        } else {
            holder.llAddComment?.visibility = View.GONE
            holder.clComment?.visibility = View.VISIBLE
            holder.tvComment?.text = run.run.comment
        }
        holder.llAddComment?.setOnClickListener {
            val intent = Intent(context, AddCommentActivity::class.java)
            intent.putExtra("run", run)
            context.startActivity(intent)
        }
    }
}