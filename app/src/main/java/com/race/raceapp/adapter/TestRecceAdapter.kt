package com.race.raceapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.race.raceapp.R
import com.race.raceapp.activity.recce.CreateNewTrackActivity
import com.race.raceapp.activity.recce.RecceTrackActivity
import com.race.raceapp.db.model.Track
import com.race.raceapp.utils.Utils
import kotlinx.android.synthetic.main.add_button_layout.view.*
import kotlinx.android.synthetic.main.item_test_recce.view.*

class TestRecceAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val FOOTER = 1

    var tracks: List<Track> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var context: Context

    constructor(context: Context) : this() {
        this.context = context
    }

    constructor(context: Context, tracks: List<Track>) : this() {
        this.context = context
        this.tracks = tracks
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == Utils.VIEW_TYPE_CELL) {
            ViewHolderCell(
                LayoutInflater.from(context).inflate(R.layout.item_test_recce, parent, false)
            )
        } else {
            ViewHolderFooter(
                LayoutInflater.from(context).inflate(R.layout.add_button_layout, parent, false)
            )
        }
    }

    override fun getItemCount(): Int = tracks.size + FOOTER

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderCell) {
            val track = tracks[position]
            holder.tvTrackNumber?.text = track.trackId.toString()
            holder.tvTrackName?.text = track.name
            holder.btnGoToTrack?.setOnClickListener {
                val intent = Intent(context, RecceTrackActivity::class.java)
                intent.putExtra("track_name", track.name)
                intent.putExtra("track", track)
                context.startActivity(intent)
            }
        }
        else if (holder is ViewHolderFooter) {
            holder.btnAdd?.setOnClickListener {
                val intent = Intent(context, CreateNewTrackActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == tracks.size) Utils.VIEW_TYPE_FOOTER else Utils.VIEW_TYPE_CELL
    }

    class ViewHolderCell(view: View) : RecyclerView.ViewHolder(view) {
        val tvTrackNumber: TextView? = view.tvTrackNumber
        val tvTrackName: TextView? = view.tvTrackName
        val btnGoToTrack: Button? = view.btnGoToTrack
    }

    class ViewHolderFooter(view: View) : RecyclerView.ViewHolder(view) {
        val btnAdd: ImageView? = view.btnAdd
    }
}