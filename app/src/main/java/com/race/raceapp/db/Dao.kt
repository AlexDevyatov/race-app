package com.race.raceapp.db

import androidx.room.*
import androidx.room.Dao
import com.race.raceapp.db.model.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

@Dao
abstract class Dao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTrack(track: Track) : Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateTrack(track: Track) : Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRun(run: Run) : Long

    @Query("SELECT * FROM tracks")
    abstract fun loadTracks() : Observable<List<Track>>

    @Transaction
    @Query("SELECT * FROM tracks")
    abstract fun loadTracksWithRunsAndSetups() : Observable<List<TrackWithRunsAndSetups>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEvent(event: Event) : Long

    @Query("SELECT * FROM events")
    abstract fun loadEvents() : Observable<List<Event>>

    @Query("SELECT * FROM events WHERE id = :id ")
    abstract fun loadEventById(id: Int?) : Observable<Event>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateRun(run: Run) : Completable

    @Transaction
    open fun insertRunAndSetup(run: Run, setup: Setup) {
        setup.runOwnerId = insertRun(run)
        insertSetup(setup)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSetup(setup: Setup) : Long

    @Query("SELECT * FROM setups")
    abstract fun loadSetups() : Observable<List<Setup>>
}