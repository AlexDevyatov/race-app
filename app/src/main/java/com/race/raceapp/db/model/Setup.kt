package com.race.raceapp.db.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(
    tableName = "setups", foreignKeys = [ForeignKey(
        entity = Run::class,
        parentColumns = arrayOf("runId"),
        childColumns = arrayOf("runOwnerId"),
        onDelete = ForeignKey.CASCADE
    )]
)
@Parcelize
data class Setup(
    @PrimaryKey(autoGenerate = true)
    var setupId: Long? = null,
    var runOwnerId: Long? = null,
    var setupName: String = "",
    var rideHeightFront: Double? = null,
    var rideHeightRear: Double? = null,
    var springsFront: Double? = null,
    var springsRear: Double? = null,
    var springLengthFront: Double? = null,
    var springLengthRear: Double? = null,
    var camberFront: Double? = null,
    var camberRear: Double? = null,
    var toeFront: String? = null,
    var toeRear: String? = null,
    var dampersModelFront: String? = null,
    var dampersModelRear: String? = null,
    var clicksReboundFront: Double? = null,
    var clicksReboundRear: Double? = null,
    var clicksBumpFront: Double? = null,
    var clicksBumpRear: Double? = null,
    var clicksFustBumpFront: Double? = null,
    var clicksFustBumpRear: Double? = null,
    var rampsFront: String? = null,
    var rampsRear: String? = null,
    var ffFront: String? = null,
    var ffRear: String? = null,
    var preloadFront: Double? = null,
    var preloadRear: Double? = null,
    var balanceFront: Double? = null,
    var balanceRear: Double? = null,
    var brakePads: String? = null,
    var fuelLevel: Double? = null,
    var tyres: String? = null,
    var temperature: Double? = null,
    var time: String? = null
) : Parcelable {
    override fun toString(): String = setupName
}