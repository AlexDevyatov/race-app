package com.race.raceapp.db.model

import androidx.room.Embedded
import androidx.room.Relation

data class TrackWithRunsAndSetups (
    @Embedded val track: Track,
    @Relation(
        entity = Run::class,
        parentColumn = "trackId",
        entityColumn = "trackOwnerId"
    )
    val runs: List<RunWithSetup>
)