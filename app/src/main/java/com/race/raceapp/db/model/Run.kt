package com.race.raceapp.db.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(
    tableName = "runs", foreignKeys = [ForeignKey(
        entity = Track::class,
        parentColumns = arrayOf("trackId"),
        childColumns = arrayOf("trackOwnerId"),
        onDelete = ForeignKey.CASCADE
    )]
)
@Parcelize
data class Run(
    @PrimaryKey(autoGenerate = true)
    var runId: Long? = null,
    var trackOwnerId: Long? = null,
    var number: Int? = null,
    var km: Double? = null,
    var timeIn: Double? = null,
    var fuelIn: Double? = null,
    var fuelAdd: Double? = null,
    var weather: String? = null,
    var tyreType: String? = null,
    var tyrePressure: String? = null,
    var comment: String? = null
) : Parcelable