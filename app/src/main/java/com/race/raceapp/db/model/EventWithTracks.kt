package com.race.raceapp.db.model

import androidx.room.Embedded
import androidx.room.Relation

data class EventWithTracks (
    @Embedded
    val event: Event,
    @Relation(
        parentColumn = "id",
        entityColumn = "eventId"
    )
    val tracks: List<Track>
)