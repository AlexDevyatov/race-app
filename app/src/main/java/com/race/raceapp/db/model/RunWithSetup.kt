package com.race.raceapp.db.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RunWithSetup (
    @Embedded
    val run: Run,

    @Relation(
        parentColumn = "runId",
        entityColumn = "runOwnerId"
    )
    val setup: Setup
) : Parcelable