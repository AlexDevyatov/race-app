package com.race.raceapp.db.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Entity(tableName = "tracks", foreignKeys = [ForeignKey(
    entity = Event::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("eventId"),
    onDelete = ForeignKey.CASCADE
)])
@Parcelize
data class Track(

    @PrimaryKey(autoGenerate = true)
    var trackId: Long? = 0,
    var name: String = "",
    var eventId: Long?
) : Parcelable