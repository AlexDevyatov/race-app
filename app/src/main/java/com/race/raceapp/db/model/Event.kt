package com.race.raceapp.db.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.race.raceapp.db.DateConverter
import kotlinx.android.parcel.Parcelize
import java.util.*

@Entity(tableName = "events")
@TypeConverters(DateConverter::class)
@Parcelize
data class Event(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = 0,
    var eventName: String = "",
    var startDate: Date = Date(),
    var finishDate: Date = Date(),
    var country: String = "",
    var region: String = ""
) : Parcelable {
    override fun toString(): String = eventName
}