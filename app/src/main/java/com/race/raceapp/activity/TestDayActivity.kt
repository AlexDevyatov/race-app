package com.race.raceapp.activity

import android.os.Bundle
import com.race.raceapp.R
import com.race.raceapp.fragment.test_day.TestDayFragment

class TestDayActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRecceTheme)
        super.onCreate(savedInstanceState)
        setTitle(R.string.test_day)
        setFragment(TestDayFragment())
    }
}
