package com.race.raceapp.activity

import android.content.Intent
import android.content.res.Resources
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.race.raceapp.R
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.custom_header.*
import kotlinx.android.synthetic.main.custom_nav_drawer.*

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        setSupportActionBar(toolbar as Toolbar)
        val sideBarToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar as Toolbar,
            R.string.open_drawer,
            R.string.close_drawer
        )
        drawerLayout.addDrawerListener(sideBarToggle)
        sideBarToggle.syncState()
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.burger_white)
        ivCloseDrawer.setOnClickListener {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        clTestDay.setOnClickListener {
            val intent = Intent(this, TestDayActivity::class.java)
            startActivity(intent)
        }
    }

    protected fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }

    protected fun setToolbarInvisible() {
        toolbar.visibility = View.GONE
    }

    fun setToolbarColor(drawable: Drawable) {
        toolbar.background = drawable
    }

    override fun setTheme(theme: Resources.Theme?) {
        super.setTheme(theme)
        setToolbarColor(ColorDrawable(resources.getColor(R.color.colorPrimaryText)))
    }
}
