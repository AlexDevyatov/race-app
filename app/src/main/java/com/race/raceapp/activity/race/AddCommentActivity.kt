package com.race.raceapp.activity.race

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.race.raceapp.R
import com.race.raceapp.db.model.Run
import com.race.raceapp.fragment.test_day.race.AddCommentFragment
import kotlinx.android.synthetic.main.activity_add_comment.*

class AddCommentActivity : AppCompatActivity() {

    lateinit var run: Run

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRaceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_comment)
        setTitle(R.string.add_comment)

        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                AddCommentFragment()
            )
            .commit()

        run = intent.getParcelableExtra("run")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
