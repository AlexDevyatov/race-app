package com.race.raceapp.activity.race

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.race.raceapp.R
import kotlinx.android.synthetic.main.activity_track_comment.*

class TrackCommentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRaceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_comment)
        setTitle(R.string.add_comment)

        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
