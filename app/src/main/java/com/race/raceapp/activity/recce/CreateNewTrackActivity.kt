package com.race.raceapp.activity.recce

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.race.raceapp.R
import com.race.raceapp.fragment.test_day.recce.CreateNewTrackFragment
import kotlinx.android.synthetic.main.activity_base.*

class CreateNewTrackActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRecceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_track)
        setSupportActionBar(toolbar as Toolbar)
        setTitle(R.string.new_track)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container,
                CreateNewTrackFragment()
            )
            .commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
