package com.race.raceapp.activity.recce

import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.race.raceapp.R
import com.race.raceapp.fragment.test_day.recce.RecceTrackFragment
import com.race.raceapp.db.model.Track
import com.race.raceapp.service.LocationService
import com.race.raceapp.utils.DistantionProvider
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.fragment_recce_track.*

class RecceTrackActivity : AppCompatActivity() {

    lateinit var track: Track
    var mayReturnTrack = true

    private var mService: LocationService? = null
    private var mReceiver: LocationReceiver? = null
    private var mBound = false
    private var mStarted = false

    private val mServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.i("LocationService", "Connected")
            val binder = service as LocationService.LocalBinder
            mService = binder.service
            mService?.requestLocationUpdates()
            mBound = true
            mStarted = true
        }

        override fun onServiceDisconnected(name: ComponentName) {

            mService = null
            mBound = false
            mStarted = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRecceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recce_track)
        setSupportActionBar(toolbar as Toolbar)
        val title = intent.getStringExtra("track_name")
        track = intent.getParcelableExtra("track")
        setTitle(title)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        setFragment(RecceTrackFragment())

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 1
            )
        }

        mReceiver = LocationReceiver()
    }

    override fun onStart() {
        super.onStart()

        if (mStarted) {
            bindService()
            registerReceiver()
        }
    }

    fun bindService() {
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(
            Intent(this, LocationService::class.java),
            mServiceConnection,
            Context.BIND_AUTO_CREATE
        )
    }

    fun registerReceiver() {
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(
            mReceiver!!,
            IntentFilter(LocationService.ACTION_BROADCAST)
        )
    }

    fun stopService() {
        mService?.removeLocationUpdates()
        mService?.stopSelf()
        mStarted = false
    }

    fun setFragment(fragment : Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
               fragment
            )
            .commit()
    }

    override fun onStop() {
        super.onStop()
        if (mBound) {
            unbindService(mServiceConnection)
            mBound = false
        }
        LocalBroadcastManager.getInstance(applicationContext).unregisterReceiver(mReceiver!!)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        DistantionProvider.clear()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class LocationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val location = intent?.getParcelableExtra(LocationService.EXTRA_LOCATION) as Location

            Log.d("DistSum", "sum = ${DistantionProvider.getMainDistantionKm()}")
            val speedKmH = DistantionProvider.lastSpeedKmH
            tvShortTrip.text = "${DistantionProvider.getShortDistantionKm()}"
            tvMainTrip.text = "${DistantionProvider.getMainDistantionKm()}"
            tvSpeed.text = "$speedKmH"
        }
    }
}
