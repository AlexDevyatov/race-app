package com.race.raceapp.activity

import android.os.Bundle
import com.race.raceapp.R
import com.race.raceapp.fragment.HomeFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.MainTheme)
        super.onCreate(savedInstanceState)
        setToolbarInvisible()
        setFragment(HomeFragment())
    }
}
