package com.race.raceapp.activity.race

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.race.raceapp.R
import com.race.raceapp.adapter.CustomFragmentPagerAdapter
import com.race.raceapp.db.model.Run
import com.race.raceapp.db.model.RunWithSetup
import com.race.raceapp.db.model.Setup
import com.race.raceapp.fragment.test_day.race.CreateRunFragment
import com.race.raceapp.fragment.test_day.race.CreateSetupFragment
import kotlinx.android.synthetic.main.activity_create_run.*

class CreateRunActivity : AppCompatActivity() {

    private lateinit var pagerAdapter: CustomFragmentPagerAdapter

    var run = Run()
    var setup = Setup()

    var editRunMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRaceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_run)

        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val trackId = intent.getLongExtra("track_id", 0)
        if (trackId != 0L) {
            run.trackOwnerId = trackId
        }

        // если мы редактируем существующий ран
        val run = intent.getParcelableExtra<RunWithSetup>("run")
        if (run != null) {
            editRunMode = true
            this.run = run.run
            this.setup = run.setup
        }

        if (editRunMode) {
            setTitle("Run ${run.run.number}")
        } else {
            setTitle(R.string.new_run)
        }

        setupViewPager()
    }

    private fun setupViewPager() {
        pagerAdapter = CustomFragmentPagerAdapter(supportFragmentManager)
        pagerAdapter.addFragment(CreateSetupFragment(), getString(R.string.setup))
        pagerAdapter.addFragment(CreateRunFragment(), getString(R.string.run_info))
        createRunViewPager.adapter = pagerAdapter

        createRunTabLayout.setupWithViewPager(createRunViewPager)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
