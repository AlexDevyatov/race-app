package com.race.raceapp.activity.race

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.race.raceapp.R
import com.race.raceapp.activity.MainActivity
import com.race.raceapp.db.AppDatabase
import com.race.raceapp.db.model.Run
import com.race.raceapp.db.model.Setup
import com.race.raceapp.fragment.test_day.race.SaveSetupFragment
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_save_setup.*

class SaveSetupActivity : AppCompatActivity() {

    private val disposable = CompositeDisposable()

    lateinit var setup: Setup
    lateinit var run: Run

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRaceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_setup)
        setTitle(R.string.new_setup)

        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        setup = intent.getParcelableExtra("setup")
        run = intent.getParcelableExtra("run")

        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                SaveSetupFragment()
            )
            .commit()
    }

    fun setSetupName(name: String) {
        setup.setupName = name
    }

    fun saveSetupAndRun() {
        val db = AppDatabase.getInstance(this)
        disposable.add(Completable.fromAction {
            db.dao().insertRunAndSetup(run, setup)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Toast.makeText(this, "Run saved successfully", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}
