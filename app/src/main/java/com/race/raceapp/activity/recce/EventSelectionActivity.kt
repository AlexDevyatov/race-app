package com.race.raceapp.activity.recce

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.race.raceapp.R
import com.race.raceapp.fragment.test_day.recce.EventSelectionFragment
import com.race.raceapp.db.model.Track
import kotlinx.android.synthetic.main.activity_base.*

class EventSelectionActivity : AppCompatActivity() {

    lateinit var track : Track

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.TestRecceTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_selection)
        setSupportActionBar(toolbar as Toolbar)
        setTitle(R.string.event_selection)

        track = intent.getParcelableExtra("track")

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                EventSelectionFragment()
            )
            .commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
